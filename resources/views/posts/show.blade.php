@extends('layouts.app')


@section('content')

	<div class="card mb-5">
		<div class="card-body">
			<h2 class="card-tilte">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created at {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>

			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					
					@method('PUT')
					@csrf

					@if($post->likes->contains("user_id", Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif

				</form>

				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
					Comment
				</button>

				<!-- Modal -->
				<form method="POST" action="/posts/{{$post->id}}/comment">

					@csrf

					<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Leave a comment</h5>
									<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
								</div>
								<div class="modal-body">
									<div class="mb-3">
										<label for="message-text" class="col-form-label">Comment:</label>
										<textarea class="form-control" id="content" name="content" rows="3"></textarea>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Comment</button>
								</div>
							</div>
						</div>
					</div>
				</form>

			@endif

			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>	
			</div>
		</div>
	</div>

	<h4 class="card-tilte">Comments:</h2>
	@if(count($post->comments) > 0)
		@foreach($post->comments as $comment)
			<div class="card mb-3">
				<div class="card-body text-center">
					<h4 class="card-text">{{$comment->content}}</h4>
					<h6 class="card-text">Posted by: {{$comment->user->name}}</h6>
					<h6 class="card-text">Posted on: {{$comment->created_at}}</h6>
				</div>
			</div>
		@endforeach
	@endif
@endsection